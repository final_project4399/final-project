#include <iostream>
#include <cassert>
#include <cctype>
#include <string>
#include <fstream>

#include "Game.h"
#include "Prompts.h"
#include "Piece.h"

using std::ofstream;
using std::string;
using std::cin;
using std::cout;
using std::endl;

Game::~Game() {

    // Delete the factories used to generate pieces
    for (size_t i = 0; i < _registered_factories.size(); i++) {
      delete _registered_factories[i];
    }
    //Delete the individual piece objects
    for(unsigned int i = 0; i < _pieces.size(); i++){
      if(_pieces[i] != nullptr)
	delete _pieces[i];
    }
    
    // Delete any other dynamically-allocated resources here
    


}

// Create a Piece on the board using the appropriate factory.
// Returns true if the piece was successfully placed on the board.
bool Game::init_piece(int piece_type, Player owner, Position pos) {
    Piece* piece = new_piece(piece_type, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!valid_position(pos)) {
        Prompts::out_of_bounds();
        return false;
    }

    // Fail if the position is occupied
    if (get_piece(pos)) {
        Prompts::blocked();
        return false;
    }
    _pieces[index(pos)] = piece;
    return true;
}

// Get the Piece at a specified Position.  Returns nullptr if no
// Piece at that Position or if Position is out of bounds.
Piece* Game::get_piece(Position pos) const {
    if (valid_position(pos))
        return _pieces[index(pos)];
    else {
        Prompts::out_of_bounds();
        return nullptr;
    }
}

// Print the appropriate character for each different piece on the screen
// Called in draw_board
void print_piece(Piece* piece){
  // get piece info
  if(piece == nullptr){
    cout << "    ";
    return;
  }  
  if(piece->owner() == WHITE){
    Terminal::color_fg(1, Terminal::BLACK);
    switch (piece->piece_type()){
    case PAWN_ENUM:
      cout << " \u2659  ";
      break;
    case KNIGHT_ENUM:
      cout << " \u2658  ";
      break;
    case BISHOP_ENUM:
      cout << " \u2657  ";
      break;
    case ROOK_ENUM:
      cout << " \u2656  ";
      break;
    case QUEEN_ENUM:
      cout << " \u2655  ";
      break;
    case KING_ENUM:
      cout << " \u2654  ";
      break;
    }
    return;
  }
  if(piece->owner() == BLACK){
    Terminal::color_fg(1, Terminal::YELLOW);
    switch (piece->piece_type()){
    case PAWN_ENUM:
      cout << " \u265F  ";
      break;
    case KNIGHT_ENUM:
      cout << " \u265E  ";
      break;
    case BISHOP_ENUM:
      cout << " \u265D  ";
      break;
    case ROOK_ENUM:
      cout << " \u265C  ";
      break;
    case QUEEN_ENUM:
      cout << " \u265B  ";
      break;
    case KING_ENUM:
      cout << " \u265A  ";
      break;
    }
  }
}


// Draw gameboard
void Game::draw_board(){
  cout << endl << "==================================" << endl;
  for(unsigned int i = _height; i > 0 ; i--){
    cout << i << " ";
    for(unsigned int j = 0; j < _width; j++){
      if((i+j)%2 == 0){
	Terminal::color_bg(Terminal::BLACK);
      }else{
	Terminal::color_bg(Terminal::CYAN);
      }
      print_piece(_pieces[index(Position(j, i-1))]);
      Terminal::set_default();
    }
    cout << endl;
  }
  Terminal::set_default();
  cout << "   ";
  for(unsigned int i = 0; i < _width; i++){
    char a = 'a';
    char b = a+i;
    cout <<  b << "   ";
  }
  cout << endl << "==================================" << endl;
}


// Perform a move from the start Position to the end Position
// The method returns an integer status where a value
// >= 0 indicates SUCCESS, and a < 0 indicates failure

//Save current state of game to a file
void Game::save_game(){
  //Ask user for filename to save
  Prompts::save_game();
  string name; //for storing saving filename
  cin >> name;
  //filestream for writing to file
  ofstream file(name);
  if(!file.is_open()){ //print error message if cannot open file
    Prompts::save_failure();
    return;
  }
  file << "chess" << endl; //this is gonna be changed in the future
  file << _turn << endl;
  for(unsigned int i = 0; i < _pieces.size(); i++){
    if(_pieces[i] != nullptr){
      file << _pieces[i]->owner() << " ";
      int x = i%_width; //x position of current piece
      char a = x + 'a'; //convert to char value for x pos
      int y = (i-x)/_width + 1; //y position of cuurent piece
      file << a << y << " " << _pieces[i]->piece_type() << endl;
    }
  }
  file.close();
}

//forfeit current game
int Game::forfeit(){
  next_turn(turn());
  Prompts::win(player_turn(),turn()-1);
  Prompts::game_over();
  exit(0);
}

//User interface
void Game::menu_input(){
  int toggle = 0;
  //infinite loop until game ends or user quits/saves
  while(1){
    Prompts::player_prompt(player_turn(), turn());
    //for storing user input
    string piece_input;
    cin >> piece_input;

    //quit game
    if(piece_input=="q"){
      exit(0);
    }
    
    //forfeit game
    else if(piece_input=="forfeit"){
      forfeit();
    }
    
    //save game
    else if(piece_input=="save"){
      save();
    }
    
    //draw board and toggle board mode
    else if(piece_input=="board"){
      toggle++;
      if (toggle % 2 != 0){
	draw_board();
      }
    }
    else if(piece_input.length() == 5){ //valid move format given
      //breakdown input into valid coordinates
      string x_1 = piece_input.substr(0,1);
      string x_2 = piece_input.substr(3,1);
      string y_1 = piece_input.substr(1,1);
      string y_2 = piece_input.substr(4,1);

      //convert coordinates
      int x_start = toupper(x_1[0])-65;
      int x_end = toupper(x_2[0])-65;
      int y_start = atoi(y_1.c_str())-1;
      int y_end = atoi(y_2.c_str())-1;
      //if move is valid, move onto next turn
      //if not, repeat prompt
      int result = make_move(Position(x_start,y_start),Position(x_end,y_end));
      //successful move is made
      //now check for check or if game is over
      if(result==SUCCESS || result==MOVE_CAPTURE || result==MOVE_PAWNTOQUEEN){
	//draw board if toggled on
	if (toggle % 2 != 0){
	  draw_board();
	}
	//
      }
    }
  }
}

// Check for check
int Game::check_check() {
  Position king;
  //look for user's king and assign to king
  for (int i = 0; i < 64; i++) {
    Piece* user_king = _pieces[i];
    if(user_king && user_king->owner()=this->player_turn() && user_king->piece_type()==5){
      //convert into usable format
      int temp = get_position(user_king);
      string king_pos = get_format_index(temp);
      int king_x = get_x(king_pos);
      int king_y = atoi(king_pos.substr(1,1).c_str())-1;
      king = Position(king_x,king_y);
    }
  }
  Position check;
  for(int i = 0; i <64;i++){
    Piece* ptr = _pieces[i];
    //look for all opponent pieces
    if(ptr && ptr->owner()!=this->player_turn()){
      int opp_temp = get_position(ptr);
      string opp_start = get_format_index(opp_temp);
      int opp_x = get_x(opp_start);
      int opp_y = atoi(start_pos.substr(1,1).c_str())-1;
      check = Position(opp_x, opp_y);
      int is_check;
    }
  }
}

  //iterate and look for check
// Search the factories to find a factory that can translate
//`piece_type' into a Piece, and use it to create the Piece.
// Returns nullptr if factory not found.
Piece* Game::new_piece(int piece_type, Player owner) {

    PieceGenMap::iterator it = _registered_factories.find(piece_type);

    if (it == _registered_factories.end()) { // not found
        std::cout << "Piece type " << piece_type << " has no generator\n";
        return nullptr;
    } else {
        return it->second->new_piece(owner);
    }
}

// Add a factory to the Board to enable producing
// a certain type of piece. Returns whether factory
// was successfully added or not.
bool Game::add_factory(AbstractPieceFactory* piece_gen) {
    // Temporary piece to get the ID
    Piece* p = piece_gen->new_piece(WHITE);
    int piece_type = p->piece_type();
    delete p;

    PieceGenMap::iterator it = _registered_factories.find(piece_type);
    if (it == _registered_factories.end()) { // not found, so add it
        _registered_factories[piece_type] = piece_gen;
        return true;
    } else {
        std::cout << "Piece type " << piece_type << " already has a generator\n";
        return false;
    }
}

